package ru.kaza.tpo3;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeRunner extends AlltestsTest {

    @Override
    MutableCapabilities getBrowserOptions() {
        return new ChromeOptions();
    }
}
