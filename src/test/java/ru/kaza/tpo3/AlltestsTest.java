package ru.kaza.tpo3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;

public abstract class AlltestsTest {
    protected static final String REMOTE_DRIVER_URL = "http://localhost:4444/wd/hub";
    protected WebDriver driver;
    JavascriptExecutor js;
    protected Duration defaultWait;

    abstract MutableCapabilities getBrowserOptions();

    @BeforeEach
    public void setUp() throws MalformedURLException {
        MutableCapabilities options = getBrowserOptions();
        options.setCapability("selenoid:options", new HashMap<String, Object>() {{
            /* How to add test badge */
            put("name", "Test badge...");
            /* How to set session timeout */
            put("sessionTimeout", "15m");
//            put("timeZone", "Europe/Moscow");
//            put("env", new ArrayList<String>() {{
//                add("LANG=ru_RU.UTF-8");
//                add("LANGUAGE=ru:en");
//                add("LC_ALL=ru_RU.UTF-8");
//            }});
            /* How to add "trash" button */
            put("labels", new HashMap<String, Object>() {{
                put("manual", "true");
            }});

            /* How to enable video recording */
            put("enableVideo", true);
        }});
        driver = new RemoteWebDriver(new URL(REMOTE_DRIVER_URL), options);
        js = (JavascriptExecutor) driver;
        defaultWait = Duration.ofSeconds(30);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    // с англ на руский ten
    @Test
    public void chooseEnglishToRussia() {
        driver.get("https://translate.google.com/");
        driver.manage().window().setSize(new Dimension(1600, 873));
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div/c-wiz/div[2]/button/div[3]")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div/div" +
                "/div[2]/input")).sendKeys("rus");
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div/div/div[4]/div/div")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div/c-wiz/div[5]/button/div[3]")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div[2]/div" +
                "/div[2]/input")).sendKeys("engl");
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div[2]/div/div[4]/div/div")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz/span/span/div/textarea")).sendKeys("восемь");
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div/span/span/span")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div")).click();
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div"), "eight"));
        }
    }


    @Test
    public void choseAutoToEnglish() {
        driver.get("https://translate.google.com/");
        driver.manage().window().setSize(new Dimension(1600, 873));
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div/c-wiz/div[5]/button/div[3]")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div[2]/div/div[2]/input")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div[2]/div" +
                "/div[2]/input")).sendKeys("engl");
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div/c-wiz/div[2]/c-wiz/div[2]/div/div[4]/div/div")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz/span/span/div/textarea")).click();
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz/span/span/div/textarea")).sendKeys("семь");
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2" +
                    "]/div[3]/c-wiz[2]/div/div[9]/div/div"), "seven"));
        }
    }


    @Test
    public void documentTranslate() {
        driver.get("https://translate.google.com/");
        driver.manage().window().setSize(new Dimension(1600, 873));
        driver.findElement(By.xpath("//span[contains(.,\'Documents\')]")).click();
        driver.findElement(By.xpath("//div[3]/c-wiz/div[2]/c-wiz/div/div/div/div")).click();
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//div[3]/c-wiz/div[2]/c-wiz/div/div/div/div"), "Choose a document"));
        }
    }

    @Test
    public void imageTranslate() {
        driver.get("https://translate.google.com/");
        driver.manage().window().setSize(new Dimension(1600, 873));
        driver.findElement(By.xpath("//span[contains(.,\'Images\')]")).click();
        driver.findElement(By.xpath("//div[5]/c-wiz/div[2]/c-wiz/div/div/div")).click();
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//div[5]/c-wiz/div[2]/c-wiz/div/div/div"), "Choose an " +
                    "image"));
        }
    }

    @Test
    public void routelanguage() throws InterruptedException {
        driver.get("https://translate.google.com/");
        driver.manage().window().setSize(new Dimension(1600, 873));
        driver.findElement(By.xpath("//button/div[3]")).click();
        driver.findElement(By.xpath("//input")).sendKeys("eng");
        driver.findElement(By.xpath("//div[4]/div/div")).click();
        driver.findElement(By.xpath("//div[5]/button/div[3]")).click();
        driver.findElement(By.xpath("//div[2]/div/div[2]/input")).sendKeys("rus");
        driver.findElement(By.xpath("//div[4]/div/div")).click();
        driver.findElement(By.xpath("//textarea")).sendKeys("ten");
        driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div")).click();
        {
            WebElement element = driver.findElement(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div"));
            Actions builder = new Actions(driver);
            builder.doubleClick(element).perform();
        }
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2]/div[3]/c-wiz[2]/div/div[9]/div/div"), "eight"));
        }
        driver.findElement(By.xpath("//div/div/span/button/div[3]")).click();
        {
            WebElement element = driver.findElement(By.tagName("body"));
            Actions builder = new Actions(driver);
            builder.moveToElement(element, 0, 0).perform();
        }
        {
            WebDriverWait wait = new WebDriverWait(driver, defaultWait);
            wait.until(ExpectedConditions.textToBe(By.xpath("//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div/div[2" +
                    "]/div[3]/c-wiz[2]/div/div[9]/div/div"), "восемь"));
        }
    }
}
