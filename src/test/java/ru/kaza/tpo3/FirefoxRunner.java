package ru.kaza.tpo3;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxRunner extends AlltestsTest {
    @Override
    MutableCapabilities getBrowserOptions() {
        return new FirefoxOptions();
    }
}
